# generator-ng-webapp

> [Yeoman](http://yeoman.io) generator


## Getting Started

```
$ npm install -g yo
```

### Yeoman Generators

Yeoman travels light. He didn't pack any generators when he moved in. You can think of a generator like a plug-in. You get to choose what type of application you wish to create, such as a Backbone application or even a Chrome extension.

To install generator-ng-webapp from npm, run:

```
$ npm install -g git://github.com/patrickhulce/ng-webapp-generator.git
```

Finally, initiate the generator:

```
$ yo ng-webapp
```

### Getting To Know Yeoman

Yeoman has a heart of gold. He's a person with feelings and opinions, but he's very easy to work with. If you think he's too opinionated, he can be easily convinced.

If you'd like to get to know Yeoman better and meet some of his friends, [Grunt](http://gruntjs.com) and [Bower](http://bower.io), check out the complete [Getting Started Guide](https://github.com/yeoman/yeoman/wiki/Getting-Started).


## About This Generator

Scaffolds a web application with

- jQuery
- Bootstrap
- FontAwesome
- Angular (and various ng-* packages)
- lodash

### Intended Usage

The intended usage is to offer a complete deployment solution from the start. This package assumes two primary modes of deployment: static or python.

#### Static Deployment

The assumption is that the site (or at least the repository) will be entirely static.
In this case, the `grunt run` command will use the `grunt-contrib-connect` server for hosting the site during development.
Deployment during production will simply be hosting the `www-root` folder after a `grunt build` on the target server.

## License

MIT
